# Testing

For image testing this project uses the
[Linaro Automated Validation Architecture(LAVA)](https://www.lavasoftware.org/).
The images are generate a gitlab-ci pipeline. This pipline builds the images and
sends them to the lava testlab.

## Test targets

following images are test:
- qemu-armhf
- qemu-arm64
- qemu-amd64
- x86-64-efi
- hikey
- beagle-bone-black

## Deploy test

After the [LAVA setup](#lava-setup) a test can be deploy with the lavacli tool, e.g.:
```
lavacli jobs run tests/jobs/xenomai-qemu-amd64.yml
```

# Tests

Currently the following tests are executed on each target:
- lava-smoketest
- xenomai-test-suite

### lava-smoketest

The lava smoke tests are part of http://git.linaro.org/lava-team/lava-functional-tests.git
and check machine data from the target.

### xenomai-test-suite

The xenomai test suite executes the xeno-test tool from xenomai/testsuite.

# Test Architecture

To test xenomai-images on the target hardware the following architecture
is used:
```
                                                               +----------+
                                                               | Target 1 |
                                                            /--|  beagle  |
+-----------+        +---------+       +---------+    /-----   |  bone    |
|           |        | LAVA    |       | LAVA    | ---         +----------+
| gitlab-   | ------ | master  |------ | Dis-    | --
| runner    |        |         |       | patcher | \ \---      +----------+
+-----------+        +---------+       +---------+  \-   \---  | Target 2 |
                                                      \      \-| x86-64   |
                                                       \       |          |
                                                        \-     +----------+
                                                          \
                                                           \   +----------+
                                                            \- | Target n |
                                                              \| qemu     |
                                                               |          |
                                                               +----------+
```
A test is deployed in the following steps:
1. gitlab-runner: builds the artifacts
2. After the build is successful the build artifacts are deployed to a AWS S3 bucket.
3. The runner sends a lava job description to the LAVA master, who triggers the
job execution on LAVA Dispatcher.

The LAVA master selects the LAVA Dispatcher to execute the given job on a
target. Qemu targets are executed directly on the LAVA Dispatcher. For non-virtual
targets the payload (kernel,rootfs,...) is deployed via tftp to the selected
hardware.

The dispatcher executes the following steps:
1. Instrumentation of the rootfs. This will collect the necessary lavatools
and adds them to the rootfs
2. Power up the target
3. deploy the payload(kernel,rootfs,...) with help of the bootloader
4. trigger the payload boot
5. execute the tests
6. power off the target

# LAVA Setup

Setup a lava environment by following the
[installation guide](https://docs.lavasoftware.org/lava/first-installation.html)
or use [lava-docker](https://github.com/kernelci/lava-docker).

# CI Files

To support different ci setups all ci files are stored in `.ci`. `.gitlab-ci.yml` in
the repository root use the settings to build on gitlab.com. To use another setup adapt
the [Custom CI configuration path](https://code.siemens.com/help/ci/pipelines/settings#custom-ci-configuration-path)


## CI Variables

The following variables are used and set by the ci system:
- Proxy settings:
  - `HTTP_PROXY`    : http proxy
  - `HTTPS_PROXY`   : https proxy
  - `FTP_PROXY`     : ftp proxy
  - `NO_PROXY`      : no proxy

- Build environment specific settings:
  - `ALT_DNS_SERVER`: optional, can be set to something like "8.8.8.8" to use a specific DNS server
  - `DISTRO_APT_PREMIRRORS`: optional, can be set to something like "deb\.debian\.org cdn-aws.deb.debian.org\n" to use a specific Debian mirror

- CI job "filers":
  - `ALL_QEMU_JOBS`: Default is false. Set to true to enable all qemu based jobs
  - `LAVA_TESTS_ENABLED`: Default is true. Set to false to disable all LAVA jobs

- LAVA settings:
  - `LAVA_MASTER_ACCOUNT`: lava master account name to register lavacli for test execution
  - `LAVA_MASTER_TOKEN`: token to connect with the lava master
  - `LAVA_ARTIFACTS_URL`: optional variable where to get the artifacts for testing
  - `LAVA_MASTER_URL`: URL to the LAVA Master

- Image deployment settings:
  - `USE_S3_BUCKET`: Set to `1` in order to upload build artifacts to AWS S3
  - `AWS_ACCESS_KEY_ID`: Key ID to use for uploading
  - `AWS_SECRET_ACCESS_KEY`: Secret access key for uploading
  - `S3_BUCKET_URL`: S3 URL where to place the artifacts (synchronize with `LAVA_ARTIFACTS_URL`)

- Build settings:
  - `BUILD_OPTIONS` : optional parameter. Used for triggers. Overwrite to build the newest ipipe together with xenomai or other combinations.

## LAVA Template variable

- `TARGET`: Name of the target in the LAVA Lab
- `BUILD_ARCH`: Architecture of the Target
- `DEPLOY_DIR`: Path to the test components will be generate by the script `scripts/run-lava-tests.sh`
- `ISAR_IMAGE`: Name of the ISAR image (e.g. demo-image)
- `ISAR_DISTRIBUTION`: Name of the ISAR DISTRIBUTION (e.g. xenomai-demo)
