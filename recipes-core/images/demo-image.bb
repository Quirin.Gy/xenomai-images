#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2018
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

inherit image

ISAR_RELEASE_CMD = "git -C ${LAYERDIR_xenomai} describe --tags --dirty --always --match 'v[0-9].[0-9]*'"
DESCRIPTION = "Xenomai demo and test image"

# The Xenomai testsuite was part of the xenomai-runtime package until 3.2.x.
# Since 3.3.x a new package (xenomai-testsuite) is providing the testsuite.
def has_testsuite_package(d):
    xeno_ver = "PREFERRED_VERSION_xenomai"
    v3_0 = d.getVar("XENOMAI_3_0_VERSION_LIST")
    v3_1 = d.getVar("XENOMAI_3_1_VERSION_LIST")
    v3_2 = d.getVar("XENOMAI_3_2_VERSION_LIST")
    all = v3_0 + " " + v3_1 + " " + v3_2

    return bb.utils.contains_any(xeno_ver, all, False, True, d)

def get_testsuite_package_names(d):
    if not has_testsuite_package(d):
        return ""

    return "xenomai-testsuite xenomai-testsuite-dbgsym"

# Install gdb only for older stable releases. Enables gdb tests and avoids a
# arch missmatch for compat builds on newer releases.
IMAGE_PREINSTALL:append:xenomai3 = " ${@ 'gdb' if not has_testsuite_package(d) else '' }"

IMAGE_PREINSTALL += " \
    bash-completion less vim nano man \
    ifupdown isc-dhcp-client net-tools iputils-ping ssh \
    iw wireless-tools wpasupplicant dbus \
    lsb-release"

IMAGE_INSTALL:append:xenomai3 = " xenomai-runtime xenomai-runtime-dbgsym"
IMAGE_INSTALL:append:xenomai3 = " ${@get_testsuite_package_names(d)}"
IMAGE_INSTALL:append:xenomai3 = " libxenomai1-dbgsym"
IMAGE_INSTALL:append:xenomai4 = " libevl"

IMAGE_INSTALL += "customizations sshd-regen-keys expand-on-first-boot"
