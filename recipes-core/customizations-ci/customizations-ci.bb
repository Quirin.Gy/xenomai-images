#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2024
#
# Authors:
#  Florian Bezdeka <florian.bezdeka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

inherit dpkg-raw

DESCRIPTION = "CI specific image customizations"

SRC_URI = " \
    file://postinst \
    "
DEBIAN_DEPENDS = "bash"

do_install() {
}
