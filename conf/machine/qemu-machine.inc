#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2018
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

IMAGE_FSTYPES ?= "ext4"

IMAGE_INSTALL:remove = "expand-on-first-boot"
ROOTFS_EXTRA = "1024"
