#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2024
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require libevl.inc

python () {
    import subprocess
    cmd = 'git ls-remote --tags --head --refs https://source.denx.de/Xenomai/xenomai4/libevl.git | ' \
          'sed -e "s/.*[[:space:]]refs\/\(tags\|heads\)\///" | grep "r[0-9]\+" | ' \
          'sort -r -n -k 1.2 | head -1'
    (ret, out) = subprocess.getstatusoutput(cmd)
    d.setVar('LATEST_RELEASE', out)
}

SRC_URI += "git://source.denx.de/Xenomai/xenomai4/libevl.git;protocol=https;branch=master;tag=${LATEST_RELEASE}"

CHANGELOG_V = "${@ d.getVar('LATEST_RELEASE').strip('r') }"
