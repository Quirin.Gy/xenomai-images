#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2021-2023
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require recipes-kernel/linux/linux-xenomai-3.inc

SRC_URI:append:amd64 = " git://github.com/xenomai-ci/ipipe-x86.git;protocol=https;nobranch=1"
SRCREV:amd64 ?= "ipipe-core-5.4.228-x86-12"

SRC_URI:append:arm64 = " git://github.com/xenomai-ci/ipipe-arm64.git;protocol=https;nobranch=1"
SRCREV:arm64 ?= "ipipe-core-5.4.180-arm64-5"

SRC_URI:append:armhf = " git://github.com/xenomai-ci/ipipe-arm.git;protocol=https;nobranch=1"
SRCREV:armhf ?= "ipipe-core-5.4.180-arm-5"

PV ?= "${@d.getVar('SRCREV').split('-')[2]}+"
