#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2021-2023
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require recipes-kernel/linux/linux-xenomai-3.inc

SRC_URI += "git://source.denx.de/xenomai/linux-dovetail.git;protocol=https;nobranch=1"

PV ?= "${@d.getVar('SRCREV').strip('-')[0].lstrip('v')}+"
