#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2023-2024
#
# Authors:
#  Clara Kowalsky <clara.kowalsky@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require recipes-kernel/linux/linux-xenomai-4-evl.inc

SRCREV ?= "v6.6-evl2-rebase"
