#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2018-2024
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

def get_commit(d):
    try:
        return bb.fetch2.get_srcrev(d).replace('AUTOINC+', '')
    except bb.fetch2.FetchError:
        return ""

COMMIT="${@get_commit(d)}"

dpkg_runbuild:prepend() {
    bbplain $(printf "%s-%s: Building revision %.20s\n" \
                     ${PN} ${PV} ${COMMIT})
}
