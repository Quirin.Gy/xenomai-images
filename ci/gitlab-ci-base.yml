#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2019 - 2023
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: MIT
#
stages:
  - build
  - test

variables:
  GIT_STRATEGY: clone
  http_proxy: "$HTTP_PROXY"
  https_proxy: "$HTTPS_PROXY"
  ftp_proxy: "$FTP_PROXY"
  no_proxy: "$NO_PROXY"
  ISAR_IMAGE: demo-image
  ISAR_DISTRIBUTION: xenomai-demo
  BUILD_IDENTIFIER: "xenomai-${XENOMAI_VERSION}_kernel-${KERNEL_VERSION}"

default:
  image: ghcr.io/siemens/kas/kas-isar:4.0

.common-config:
  before_script:
    - if [ -n "$ALT_DNS_SERVER" ]; then
          sudo sh -c "echo 'nameserver $ALT_DNS_SERVER' > /etc/resolv.conf";
      fi
    - mkdir -p -m=700 ~/.ssh
    - if [ -n "$https_proxy" ]; then
          echo "ProxyCommand socat - PROXY:$(echo $https_proxy | sed 's|.*://\([^:]*\).*|\1|'):%h:%p,proxyport=$(echo $https_proxy | sed 's|.*:\([0-9]*\)$|\1|')" >> ~/.ssh/config;
          chmod 600 ~/.ssh/config;
      fi

.build:
  extends: .common-config
  stage: build
  script:
    - echo "Building kas.yml:board-${TARGET}.yml${XENOMAI_BUILD_OPTION}${LINUX_BUILD_OPTION}${BUILD_OPTIONS}:opt-ci.yml"
    - kas build kas.yml:board-${TARGET}.yml${XENOMAI_BUILD_OPTION}${LINUX_BUILD_OPTION}${BUILD_OPTIONS}:opt-ci.yml
    - du -sh build/downloads/deb build/downloads/git build/downloads
    - if [ -n "${USE_S3_BUCKET}" ]; then scripts/deploy_to_aws.sh ${TARGET}; fi
  cache:
    key: downloads-${CACHE_ARCH}-${CACHE_KERNEL}
    paths:
      - build/downloads

.build-qemu:
  extends: .build
  rules:
    - if: '$KERNEL_VERSION == $LATEST_KERNEL_VERSION'
    - if: '$ALL_QEMU_JOBS == "true"'

.test:
  extends: .common-config
  stage: test
  script:
    - scripts/install-lavacli.sh
    - scripts/run-lava-tests.sh ${TARGET}
  variables:
    BUILD_JOB_NAME: "build-${BUILD_IDENTIFIER}:${TARGET}"
  artifacts:
    when: always
    reports:
      junit:
        - report.xml
    expire_in: 1 week
  rules:
    - if: '$LAVA_TESTS_ENABLED == "true"'

.test-qemu:
  extends: .test
  rules:
    - if: '$LAVA_TESTS_ENABLED != "true"'
      when: never
    - if: '$KERNEL_VERSION == $LATEST_KERNEL_VERSION'
    - if: '$ALL_QEMU_JOBS == "true"'

.build:qemu-amd64:
  extends: .build-qemu
  variables:
    TARGET: qemu-amd64
    CACHE_ARCH: amd64

.lava-test:qemu-amd64:
  extends: .test-qemu
  variables:
    TARGET: qemu-amd64

.build:qemu-armhf:
  extends: .build-qemu
  variables:
    TARGET: qemu-armhf
    CACHE_ARCH: armhf

.lava-test:qemu-armhf:
  extends: .test-qemu
  variables:
    TARGET: qemu-armhf

.build:qemu-arm64:
  extends: .build-qemu
  variables:
    TARGET: qemu-arm64
    CACHE_ARCH: arm64

.lava-test:qemu-arm64:
  extends: .test-qemu
  variables:
    TARGET: qemu-arm64

.build:hikey:
  extends: .build
  variables:
    TARGET: hikey
    CACHE_ARCH: arm64

.lava-test:hikey:
  extends: .test
  resource_group: board-hikey
  variables:
    TARGET: hikey

.build:beagle-bone-black:
  extends: .build
  variables:
    TARGET: beagle-bone-black
    CACHE_ARCH: armhf

.lava-test:beagle-bone-black:
  extends: .test
  resource_group: board-beagle-bone-black
  variables:
    TARGET: beagle-bone-black

.build:x86-64-efi:
  extends: .build
  variables:
    TARGET: x86-64-efi
    CACHE_ARCH: amd64

.lava-test:x86-64-efi:
  extends: .test
  resource_group: board-x86-64-efi
  variables:
    TARGET: x86-64-efi
